# Android Bluetooth Serial Service #

### What is this repository for? ###

* This is an adaptation of the Android Bluetooth Serial Service used by BlueTerm from pysmades.es, decoupled from any project specific dependencies so it can easily be implemented into any Bluetooth Android project
* Version 1.03

### Implementation ###

*See [BlueTooth RTC](https://bitbucket.org/berkanotechnologies/droiduino-bt-rtc-1.0) for an example of an implementation*

1. Create a sub-class which extends Android.os.Handler in order to handle messages between your app and the Bluetooth Serial Service.  In the above example it calls the hanldeMessage() activity which conditionally processes the content of the message.  
2. Instantiate the BTSerialService object and pass it the handler as its parameter.
3. Use the service's connect(*DEVICE ADDRESS*) method to connect to you Bluetooth device.
4. Call the service's start() method to start the service.
5. Call the service's write() method to send a message through Bluetooth.
6. Check the .what property of the message handler for the value *BTSerialService.MESSAGE_READ* in order to read received messages.
7. Call the service's stop() method to stop the service.